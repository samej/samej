#!/usr/bin/env bash

# ==================================================
#          CREATED: Mon 30/Jan/2023 - 14h
#      LAST CHANGE: Mon 20/may/2024 - 21h56min
# THIS SCRIPT AIMS: Archcraft post-install script
#           AUTHOR: Samej Spenser
#             SITE: https://linktr.ee/samej
#          TWITTER: @SamejSpenser
#         TELEGRAM: https://SamejSpenser.t.me
# ==================================================

# -------------- CORRECT KEYCHAIN SIGNATURES -------------- #

## Run these commands to fix (https://t.me/archcraftos/39315):

sudo pacman-key --populate archlinux

sudo pacman -Sy archlinux-keyring

## Parallel downloads

### 1. The first step is to access the pacman configuration file using your favorite text editor:

sudo nano /etc/pacman.conf

### 2. After accessing the pacman configuration file, look for the line "ParallelDownloads = 8" and uncomment it (removing the “#” in front):

### 3. Finally, synchronize the repositories with the command:

sudo pacman -Syyu

# --------------------- AUR --------------------- #

# Installing yay, curl, and wget
sudo pacman -S yay curl wget --noconfirm

# Download Arch Linux installer with my programs
programas_wget=(
  https://gitlab.com/samej/dotfiles/-/blob/5e5ff41f35f0983ce00586bfafc82fbd7301df00/.zsh_aliases
  https://gitlab.com/samej/dotfiles/-/blob/6f1b53b92afab8452c0f4d472fd0fe0543a40bff/.bspwm.conkyrc
  https://gitlab.com/samej/dotfiles/-/blob/8fca925276f79eac143453837d49039bc07ac4bf/.bash_aliases
  #https://gitlab.com/samej/samej/-/blob/2601ea839a59412e92459d9a586515f881e6a56f/en_install-prog_arch-linux.sh
  #https://gitlab.com/samej/samej/-/blob/2601ea839a59412e92459d9a586515f881e6a56f/pt-br_install-prog_arch-linux.sh
)

# Download files
wget -c "${programas_wget[@]}"

# Edit .zshrc
sudo nano ~/.zshrc

# ADIÇÕES DE SAMEJ SPENSER ~ INÍCIO #

## Alias definitions.
## You may want do put all your additions into a separate file like
## ~/.bash_aliases, instead of adding them here diretcly.
## See /usr/share/doc/bash-doc/examples in the bash-doc package.

#if [ -f ~/.bash_aliases ]; then
#    . ~/.bash_aliases
#fi

# if [ -f ~/.zsh_aliases ]; then
#     . ~/.zsh_aliases
# fi

## Utilizar o micro no lugar do nano
# alias nano='micro'

## Atualizar os Aliases
#alias al='source ~/.bash_aliases'
# alias al='source ~/.zsh_aliases'

#alias acb='geany ~/.bash_aliases'
#alias acz='geany ~/.zsh_aliases'

# alias acb='micro ~/.bash_aliases'
# alias acz='micro ~/.zsh_aliases'

#alias acb='mousepad ~/.bash_aliases'
#alias acz='mousepad ~/.zsh_aliases'

#alias acb='nano ~/.bash_aliases'
#alias acz='nano ~/.zsh_aliases'

## Iniciar o zoxide (alternativa ao comando 'cd') na inicialização
# eval "$(zoxide init zsh)"

# eval "$(direnv hook zsh)"

# ADIÇÕES DE SAMEJ SPENSER ~ FINAL #

# List of programs to be installed
programs_pacman=(
  ansible                        # Automation tool
  appimagelauncher               # Support for AppImage
  aspell                         # Spell checker
  aspell-en                      # English dictionary for aspell
  aspell-pt                      # Portuguese dictionary for aspell
  authy                          # Two-factor authentication
  bashtop                        # System resource monitor
  bat                            # Syntax-highlighted code viewer
  calcurse                       # Calendar for terminals
  clipit                         # Clipboard manager
  conky                          # A system monitor software
  copyq                          # Clipboard manager
  discord                        # Chat application for communication
  exa                            # Modern replacement for "ls"
  flameshot-git                  # Screenshot tool
  foliate                        # eBook reader
  galculator                     # Graphical calculator
  gcalcli                        # Calendar for terminals
  git                            # Versioning control
  goldendict-git                 # Dictionary and translator
  gpodder                        # Podcast client
  hunspell-en_us                 # English spell checker
  hunspell-pt-br                 # Portuguese spell checker
  hyphen-en                      # Hyphenation in English
  jitsi-meet                     # Video conferencing platform
  jrnl                           # Command-line personal journal
  kdeconnect                     # Integration between KDE devices
  kdenlive                       # Non-linear video editor
  languagetool                   # Grammar checker
  languagetool-code-comments-bin # Code comments support in LanguageTool
  languagetool-word2vec-pt       # Portuguese support in Word2Vec in LanguageTool
  less                           # Paginated text viewer
  man-pages-pt_br                # Manual pages in Portuguese
  micro                          # Intuitive text editor
  mousepad                       # Lightweight text editor
  mp3tag                         # Audio tag editor
  mythes-en                      # Thesaurus dictionary in English
  mythes-pt-br                   # Thesaurus dictionary in Portuguese
  ncdu                           # Utility for analyzing disk usage
  python-pip                     # Latest version of Python
  python-pipx                    # Python Applications in Isolated Environments
  ranger                         # A VIM-inspired filemanager for the console
  reaper                         # Digital audio workstation
  redshift                       # Automatic color temperature adjustment
  screenfetch                    # System information tool
  simplescreenrecorder           # Screen recorder
  tilix                          # Tiling terminal emulator
  tldr                           # Simplified man pages
  topgrade                       # Package updater
  ttf-droid                      # Droid fonts
  ttf-symbola                    # Fonts with different symbols
  vlc                            # Media player
  xfce4-screenshooter            # Screenshot tool for XFCE
  xfce4-taskmanager              # Task manager for XFCE
  zoom                           # Video conferencing platform
)

# Installing programs
sudo pacman -S "${programs_pacman[@]}" --noconfirm

# Once installed it will be necessary to copy the Conky default configuration file to our home directory. This in order not to have to start writing it from scratch.

cp /etc/conky/conky.conf ~ / .conkyrc

# List of programs to be installed
programs_yay=(
  castero                        # A TUI app to listen to podcast
  gfeeds-git                     # Feed/RSS Reader
  icdiff                         # Improved colored diff
  librewolf-bin                  # LibreWolf (Firefox Fork)
  localsend-bin                  # Share files to nearby devices.
  vscodium-bin                   # VSCodium IDE
)

# Installing programs
yay -S "${programs_yay@}" --noconfirm --needed

echo "
  Installation complete!"

# ---------------- APPIMAGE ---------------- #

### Create “/opt” directory in the home folder:
mkdir -p ~/opt

### Open the “/opt” directory:
cd ~/opt/

# List of AppImage's to be downloaded
programas_appimage=(
  https://github.com/Automattic/simplenote-electron/releases/download/v2.21.0/Simplenote-linux-2.21.0-x86_64.AppImage                     # Simplenote
  https://github.com/obsidianmd/obsidian-releases/releases/download/v1.5.12/Obsidian-1.5.12.AppImage                                    # Obsidian
  https://github.com/pulsar-edit/pulsar/releases/download/v1.117.0/Linux.Pulsar-1.117.0.AppImage                               # Pulsar Editor
  #https://releases.fontba.se/linux/FontBase-2.19.4.AppImage  # FontBase — use Google fonts in a single click.
  https://vault.bitwarden.com/download/?app=desktop&platform=linux&variant=appimage                                            # Bitwarden
  #https://www.pcloud.com/how-to-install-pcloud-drive-linux.html?download=electron-64   # pCloud
)

# Downloading the AppImage's
wget -c "${programas_appimage[@]}"

echo "
  Installation complete!"

## Espanso
### https://espanso.org/docs/install/linux/#appimage-x11

### Download the AppImage inside it
wget -O ~/opt/Espanso.AppImage 'https://github.com/federico-terzi/espanso/releases/download/v2.2.1/Espanso-X11.AppImage'

### Make it executable
chmod u+x ~/opt/*.AppImage

### Create the "espanso" command alias
sudo ~/opt/Espanso.AppImage env-path register

### From now on, you should have the espanso command available in the terminal (you can verify by running espanso --version).

### At this point, you are ready to use espanso by registering it first as a Systemd service and then starting it with:

### Register espanso as a systemd service (required only once)
espanso service register

### Start espanso
espanso start

# ---------------------- COMPILATIONS ---------------------- #

## Calibre
sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sudo sh /dev/stdin

## Sublime Text
curl -O https://download.sublimetext.com/sublimehq-pub.gpg && sudo pacman-key --add sublimehq-pub.gpg && sudo pacman-key --lsign-key 8A8F901A && rm sublimehq-pub.gpg

echo -e "\n[sublime-text]\nServer = https://download.sublimetext.com/arch/stable/x86_64" | sudo tee -a /etc/pacman.conf

sudo pacman -Syu sublime-text --noconfirm --needed

# -------------- INSTALLING FLATPAK PACKAGES -------------- #

# List of flatpaks to be installed
programas_flatpak=(
  #com.discordapp.Discord                   # Discord
  #com.dropbox.Client                       # Dropbox
  #com.github.bajoja.indicator-kdeconnect   # KDE Connect Indicator
  #com.github.kmwallio.thiefmd              # ThiefMD (The markdown editor worth stealing.)
  #com.vscodium.codium                      # VSCodium
  #im.riot.Riot                             # Element (mensageiro Matrix)
  #io.github.prateekmedia.appimagepool      # AppImage Pool
  #io.github.wereturtle.ghostwriter         # GhostWriter (Distraction-free text editor for Markdown)
  life.bolls.bolls                         # Bolls Bible
  #org.flameshot.Flameshot                  # Flameshot (Powerful and simple to use screenshot software)
  #org.gabmus.gfeeds                        # Feed (leitor de feeds RSS)
  org.gabmus.notorious                     # Notorious (keyboard centric notes)
  #org.gnome.gitlab.somas.Apostrophe        # Apostrophe (markdown editor semelhante ao Typora)
  #org.goldendict.GoldenDict                # GoldenDict
  #org.gpodder.gpodder                      # gPodder (Media aggregator and podcast client)
  org.hlwd.sonofman                         # BibleMultiTheSonOfMan (Bible in the Terminal)
  #org.jitsi.jitsi-meet                     # Jitsi Meet
  #org.kde.kasts                            # Kasts (Podcast application for mobile devices)
  #org.kde.kdenlive                         # KDE Connect Indicator
)

# Installing the flatpaks
flatpak install "${programas_flatpak[@]}" -y

echo "
  Flatpaks installed!"

# -------------------------- POST-INSTALLATION -------------------------- #

## Completion and update ##

topgrade

echo "INSTALLATION, UPGRADE AND UPDATE FINISHED!"

# ---------------------------------------------------------------------- #


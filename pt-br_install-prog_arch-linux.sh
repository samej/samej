#!/usr/bin/env bash

# ==================================================
#          CREATED: Mon 30/Jan/2023 - 14h
#      LAST CHANGE: Mon 20/May/2024 - 21h56min
# THIS SCRIPT AIMS: Archcraft post-install script
#           AUTHOR: Samej Spenser
#             SITE: https://linktr.ee/samej
#          TWITTER: @SamejSpenser
#         TELEGRAM: https://SamejSpenser.t.me
# ==================================================

# ------- CORRIGIR ASSINATURAS DO CHAVEIRO ------- #

## Run these commands to fix (https://t.me/archcraftos/39315):

sudo pacman-key --populate archlinux

sudo pacman -Sy archlinux-keyring

## Downloads paralelos

### 1. O primeiro passo é acessar o arquivo de configuração do pacman utilizando o seu editor de texto favorito:

sudo nano /etc/pacman.conf

### 2. Após acessar o arquivo de configuração do pacman, procure pela linha “ParallelDownloads = 8” e descomente (retirando a “#” da frente):

### 3. Por fim, sincronize os repositórios com o comando:

sudo pacman -Syyu

# --------------------------- AUR --------------------------- #

# Instalando yay, curl e wget
sudo pacman -S yay curl wget --noconfirm

# Baixando instaladores do Arch Linux com meus programas
programas_wget=(
  https://gitlab.com/samej/dotfiles/-/blob/5e5ff41f35f0983ce00586bfafc82fbd7301df00/.zsh_aliases
  https://gitlab.com/samej/dotfiles/-/blob/6f1b53b92afab8452c0f4d472fd0fe0543a40bff/.bspwm.conkyrc
  https://gitlab.com/samej/dotfiles/-/blob/8fca925276f79eac143453837d49039bc07ac4bf/.bash_aliases
  #https://gitlab.com/samej/samej/-/blob/2601ea839a59412e92459d9a586515f881e6a56f/en_install-prog_arch-linux.sh
  #https://gitlab.com/samej/samej/-/blob/2601ea839a59412e92459d9a586515f881e6a56f/pt-br_install-prog_arch-linux.sh
)

# Baixando os Arquivos
wget -c "${programas_wget[@]}"

# Editar .zshrc
sudo nano ~/.zshrc

# ADIÇÕES DE SAMEJ SPENSER ~ INÍCIO #

## Alias definitions.
## You may want do put all your additions into a separate file like
## ~/.bash_aliases, instead of adding them here diretcly.
## See /usr/share/doc/bash-doc/examples in the bash-doc package.

#if [ -f ~/.bash_aliases ]; then
#    . ~/.bash_aliases
#fi

# if [ -f ~/.zsh_aliases ]; then
#     . ~/.zsh_aliases
# fi

## Utilizar o micro no lugar do nano
# alias nano='micro'

## Atualizar os Aliases
#alias al='source ~/.bash_aliases'
# alias al='source ~/.zsh_aliases'

#alias acb='geany ~/.bash_aliases'
#alias acz='geany ~/.zsh_aliases'

# alias acb='micro ~/.bash_aliases'
# alias acz='micro ~/.zsh_aliases'

#alias acb='mousepad ~/.bash_aliases'
#alias acz='mousepad ~/.zsh_aliases'

#alias acb='nano ~/.bash_aliases'
#alias acz='nano ~/.zsh_aliases'

## Iniciar o zoxide (alternativa ao comando 'cd') na inicialização
# eval "$(zoxide init zsh)"

# eval "$(direnv hook zsh)"

# ADIÇÕES DE SAMEJ SPENSER ~ FINAL #

# Lista de programas a serem instalados
programas_pacman=(
  ansible                        # Ferramenta de automação
  appimagelauncher               # Suporte para AppImage
  aspell                         # Verificador ortográfico
  aspell-en                      # Dicionário inglês para aspell
  aspell-pt                      # Dicionário português para aspell
  authy                          # Autenticação de dois fatores
  bashtop                        # Monitor de recursos do sistema
  bat                            # Visualizador de código com destaque de sintaxe
  calcurse                       # Calendário para terminais
  clipit                         # Gerenciador de área de transferência
  conky                          # Um software de monitoramento de sistema
  copyq                          # Gerenciador de área de transferência
  discord                        # Aplicativo de chat para comunicação
  exa                            # Substituto moderno para "ls"
  flameshot-git                  # Ferramenta de captura de tela
  foliate                        # Leitor de eBooks
  galculator                     # Calculadora gráfica
  gcalcli                        # Calendário para terminais
  git                            # Controle de versionamento
  goldendict-git                 # Dicionário e tradutor
  gpodder                        # Cliente de podcast
  hunspell-en_us                 # Verificador ortográfico em inglês
  hunspell-pt-br                 # Verificador ortográfico em português
  hyphen-en                      # Hifenização em inglês
  jitsi-meet                     # Plataforma de videoconferência
  jrnl                           # Diário pessoal de linha de comando
  kdeconnect                     # Integração entre dispositivos KDE
  kdenlive                       # Editor de vídeo não linear
  languagetool                   # Verificador gramatical
  languagetool-code-comments-bin # Suporte para comentários de código no LanguageTool
  languagetool-word2vec-pt       # Suporte para português no Word2Vec no LanguageTool
  less                           # Visualizador de texto paginado
  man-pages-pt_br                # Páginas de manual em português
  micro                          # Editor de texto intuitivo
  mousepad                       # Editor de texto leve
  mp3tag                         # Editor de tags de áudio
  mythes-en                      # Dicionário thesaurus em inglês
  mythes-pt-br                   # Dicionário thesaurus em português
  ncdu                           # Utilitário para análise de uso de disco
  python-pip                     # Última versão do Python
  python-pipx                    # Aplicativos Python em ambientes isolados
  ranger                         # Um gerenciador de arquivos (inspirado no VIM) para o console
  reaper                         # Estação de trabalho de áudio digital
  redshift                       # Ajuste automático de temperatura de cor
  screenfetch                    # Ferramenta para exibir informações do sistema
  simplescreenrecorder           # Gravador de tela
  tilix                          # Emulador de terminal tiling
  tldr                           # Páginas de manual simplificadas
  topgrade                       # Atualizador de pacotes
  ttf-droid                      # Fontes Droid
  ttf-symbola                    # Fontes com símbolos diversos
  vlc                            # Reprodutor de mídia
  xfce4-screenshooter            # Captura de tela para XFCE
  xfce4-taskmanager              # Gerenciador de tarefas para XFCE
  zoom                           # Plataforma de videoconferência
)

# Instalando os programas em ordem alfabética
sudo pacman -S "${programas_pacman[@]}" --noconfirm

# Uma vez instalado, será necessário copiar o arquivo de configuração padrão do Conky para nosso diretório inicial. Isso para não ter que começar a escrever do zero.

cp /etc/conky/conky.conf ~ / .conkyrc

# Lista de programas a serem instalados
programas_yay=(
  castero                        # Um aplicativo TUI para ouvir podcast
  gfeeds-git                     # Leitor de Feeds/RSS
  icdiff                         # Diff colorido melhorado
  librewolf-bin                  # LibreWolf (Fork do Firefox)
  localsend-bin                  # Compartilhe arquivos com dispositivos próximos.
  vscodium-bin                   # IDE VSCodium
)

# Instalando os programas em ordem alfabética
yay -S "${programas_yay[@]}" --noconfirm --needed

echo "
  Instalação concluída!"

# ----------------------- APPIMAGE ----------------------- #

### Criar diretório “/opt” na pasta home:
mkdir -p ~/opt

### Abrir o diretório “/opt”:
cd ~/opt/

# Lista de AppImage's a serem baixados
programas_appimage=(
  https://github.com/Automattic/simplenote-electron/releases/download/v2.21.0/Simplenote-linux-2.21.0-x86_64.AppImage                     # Simplenote
  https://github.com/obsidianmd/obsidian-releases/releases/download/v1.5.12/Obsidian-1.5.12.AppImage                                    # Obsidian
  https://github.com/pulsar-edit/pulsar/releases/download/v1.117.0/Linux.Pulsar-1.117.0.AppImage                               # Pulsar Editor
  #https://releases.fontba.se/linux/FontBase-2.19.4.AppImage  # FontBase — use Google fonts in a single click.
  https://vault.bitwarden.com/download/?app=desktop&platform=linux&variant=appimage                                            # Bitwarden
  #https://www.pcloud.com/how-to-install-pcloud-drive-linux.html?download=electron-64   # pCloud
)

# Baixando os AppImage's
wget -c "${programas_appimage[@]}"

echo "
  Instalação concluída!"

## Espanso
### https://espanso.org/docs/install/linux/#appimage-x11

### Download the AppImage inside it
wget -O ~/opt/Espanso.AppImage 'https://github.com/federico-terzi/espanso/releases/download/v2.2.1/Espanso-X11.AppImage'

### Make it executable
chmod u+x ~/opt/*.AppImage

### Create the "espanso" command alias
sudo ~/opt/Espanso.AppImage env-path register

### From now on, you should have the espanso command available in the terminal (you can verify by running espanso --version).

### At this point, you are ready to use espanso by registering it first as a Systemd service and then starting it with:

### Register espanso as a systemd service (required only once)
espanso service register

### Start espanso
espanso start

# ---------------------- COMPILAÇÕES ---------------------- #

## Calibre
sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sudo sh /dev/stdin

## Sublime Text
curl -O https://download.sublimetext.com/sublimehq-pub.gpg && sudo pacman-key --add sublimehq-pub.gpg && sudo pacman-key --lsign-key 8A8F901A && rm sublimehq-pub.gpg

echo -e "\n[sublime-text]\nServer = https://download.sublimetext.com/arch/stable/x86_64" | sudo tee -a /etc/pacman.conf

sudo pacman -Syu sublime-text --noconfirm --needed

# -------------------- INSTALANDO PACOTES FLATPAK -------------------- #

# Lista de programas a serem instalados
programas_flatpak=(
  #com.discordapp.Discord                   # Discord
  #com.dropbox.Client                       # Dropbox
  #com.github.bajoja.indicator-kdeconnect   # KDE Connect Indicator
  #com.github.kmwallio.thiefmd              # ThiefMD (The markdown editor worth stealing.)
  #com.vscodium.codium                      # VSCodium
  #im.riot.Riot                             # Element (mensageiro Matrix)
  #io.github.prateekmedia.appimagepool      # AppImage Pool
  #io.github.wereturtle.ghostwriter         # GhostWriter (Distraction-free text editor for Markdown)
  life.bolls.bolls                         # Bolls Bible
  #org.flameshot.Flameshot                  # Flameshot (Powerful and simple to use screenshot software)
  #org.gabmus.gfeeds                        # Feed (leitor de feeds RSS)
  org.gabmus.notorious                     # Notorious (keyboard centric notes)
  #org.gnome.gitlab.somas.Apostrophe        # Apostrophe (markdown editor semelhante ao Typora)
  #org.goldendict.GoldenDict                # GoldenDict
  #org.gpodder.gpodder                      # gPodder (Media aggregator and podcast client)
  org.hlwd.sonofman                         # BibleMultiTheSonOfMan (Bíblia no Terminal)
  #org.jitsi.jitsi-meet                     # Jitsi Meet
  #org.kde.kasts                            # Kasts (Podcast application for mobile devices)
  #org.kde.kdenlive                         # KDE Connect Indicator
)

# Instalando os programas em ordem alfabética
flatpak install "${programas_flatpak[@]}" -y

echo "
  Flatpaks instalados!"

# ----------------------------- PÓS-INSTALAÇÃO ----------------------------- #

## Finalização e atualização ##

topgrade

echo "INSTALAÇÃO, UPGRADE E UPDATE FINALIZADOS!"

# ---------------------------------------------------------------------- #

